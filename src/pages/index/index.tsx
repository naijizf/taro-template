import { Component, useState } from 'react'
import { View, Input, Text, Button, EventProps, Checkbox, Label, CheckboxGroup } from '@tarojs/components'
import './index.css'
import { InputProps } from '@tarojs/components/types/Input'
import { CheckboxProps } from '@tarojs/components/types/Checkbox'

export default () => {
  const [newTodoText, setNewTodoText] = useState('')
  const [todoList, setTodoList] = useState<{
    text: string
    isComplete: boolean
  }[]>([])

  const handleInput: InputProps['onInput'] = (e) => {
    setNewTodoText(e.detail.value)
  }
  const handleClick: EventProps['onClick'] = () => {
    setTodoList(previewList => {
      console.log('previewList is ', previewList)
      if (newTodoText.length ===0) {
        return previewList
      }
      
      return [...previewList, {
        text: newTodoText,
        isComplete: false
      }]
    })
    setNewTodoText('')
  }
  const handleCheckbox: CheckboxProps['onChange'] = (e) => {
    setTodoList(prevList => {
      return prevList.map(item => {
        return {
          text: item.text,
          isComplete: e.detail.value.indexOf(item.text) > -1
        }
      })
    })
  }
  return (
    <View className={'container'}>
      <CheckboxGroup onChange={handleCheckbox}>
        {todoList.map(item => {
          return (
            <View key={item.text}>
              <Label className={item.isComplete?'complete':''}>
              <Checkbox value={item.text}></Checkbox>
              <Text>{item.text}</Text>
              </Label>
            </View>
          )
        })}
      </CheckboxGroup>
      <Input value={newTodoText} onInput={handleInput} />
      <Button onClick={handleClick}>add toto</Button>
    </View>
  )
}

// export default class Index extends Component {

//   componentWillMount () { }

//   componentDidMount () { }

//   componentWillUnmount () { }

//   componentDidShow () { }

//   componentDidHide () { }

//   render () {
//     return (
//       <View className='index'>
//         <Text>Hello world!</Text>
//       </View>
//     )
//   }
// }
